\## Hintergrund

\### Allgemeineres Problem

Wie bereits ausführlich beschrieben \[@\], deuten mehrere Studien darauf
hin, dass Patienten in Diskussionen über ihre Gesundheit, Diagnostik und
Therapie gerne in ausreichendem Maße einbezogen werden wollen \[@\].

\### Fokussiertes Problem-Statement: Gap und möglicher Fortschritt

Unseres Wissens gibt es nur sehr wenige Daten über die Risikokompetenz
von Patienten, obwohl sie als Betroffene eine wichtige Rolle im
Gesundheitswesen spielen.

\### Fokussierte Forschungsfrage/n

Wir haben eine Studie mit Patienten durchgeführt, um die folgenden
Fragen zu untersuchen:

1\. Was ist \...

2\. Warum sind \...

Umwandlung in md.

Literatur Test

1\. Quote 1 \[@cook2007\]

2\. Quote 2 \[@cook2010\]

3\. Quote 3 \[@cook2007a\]

4\. Quote 4 \[@hattie2023\]

References

Cook et al., \"A systematic review of titles and abstracts of
experimental studies in medical education: many informative elements
missing\", 2007

Cook et al., \"Instructional methods and cognitive and learning styles
in web-based learning: report of two randomised trials\", 2007

Cook et al., \"Instructional design variations in internet-based
learning for health professions education: a systematic review and
meta-analysis\", 2010

Hattie, \"Visible Learning: The Sequel: A Synthesis of Over 2,100
Meta-Analyses Relating to Achievement\", 2023
