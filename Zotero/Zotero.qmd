---
title: "Zotero"
format: html
editor: visual
#output:
  #word_document:
    #reference_docx: style_template.docx
---

# Zotero Anleitung

## Zotero in OnlyOffice

Installiere plugin Zotero

![Zotero Plugin](Bilder/onlyoffice.png)

![Zotero API-Key](Bilder/onlyoffice2.png)

## Zotero in R Studio installieren

Installier Zotero:

`https://www.zotero.org`

Zotero als biblothek für papers nutzen.

Um Zotero in RStudio leichter zu verwenden:

`https://gsverhoeven.github.io/post/zotero-rmarkdown-csl/`

Zuerst muss das `rbbt`-Paket mit dem folgenden Befehl installieren:

`remotes::install_github("paleolimbot/rbbt")`

Nach der Installation und einem Neustart von RStudio findet man das rbbt-Addin unter "Addins".

Da das Zitieren von Quellen eine häufige Aktivität beim Verfassen eines Papiers ist, sollte eine Tastenkombination dafür festgelegt werden. Vorschlag: CTRL + K.

So kann rbbt mit einer bestimmten Tastenkombination verbunden werden:

In RStudio "Tools" -\> "Modify Keyboard Shortcuts" wählen. Zotero eingeben, um das Zotero-Plugin herauszufiltern. Auf das Feld "Shortcut" in der Zeile "Insert Zotero citation" klicken. Die gewünschte Tastenkombination eingeben. Nun kann in RStudio CTRL + K gedrückt, der Name des ersten Autors eingegeben, die Zitierung ausgewählt und Enter gedrückt werden, um die Zitierungs-ID in das .Rmd-Dokument einzufügen.

Better BibTeX Better BibTeX (BBT) ist ein Plugin für Zotero und Juris-M, das die Verwaltung bibliografischer Daten erleichtert, besonders für Personen, die Dokumente mit textbasierten Toolchains verfassen (z.B. LaTeX / Markdown).

Von der Seite folgende Datei herunterladen:

`https://github.com/retorquere/zotero-better-bibtex/releases/tag/v6.7.202`

Datei: `zotero-better-bibtex-6.7.202.xpi`

Installation: 1. Im Hauptmenü zu Werkzeuge -\> Add-ons gehen. „Erweiterungen“ auswählen. 2. Auf das Zahnrad in der oberen rechten Ecke klicken und „Add-on aus Datei installieren…“ wählen. 3. Die heruntergeladene .xpi-Datei auswählen und auf „Installieren“ klicken. 4. Zotero neu starten.

Nach der Erstinstallation wird das Plugin automatisch auf neuere Versionen aktualisiert, sodass dieser Vorgang nur einmal durchgeführt werden muss.

### Zum Bearbeiten der Präferenzen:

Kann nur in .qmd oder .md Dateien verwendet werden.

Werkzeug -\> Better Bibtex -\> open preferences

![Pfad](Bilder/Werkzeug.png)

Dort wird die zurzeit benutzte Key-Wörter angezeigt. Dies lässt sich bearbeiten.

![BetterBib](Bilder/betterbib.png)

Das Standardmuster für die Schlüsselgenerierung ist

`auth.lower + shorttitle(3,3) + year`.

Wenn Arbeiten Schlüssel verwenden, die vom Standardschlüsselgenerator der Zotero Bib(La)TeX-Exporte erzeugt wurden, sollte stattdessen zotero.clean verwendet werden, um die Migration von bestehenden Exporten für Benutzer, die zuvor die Standard-Zotero Bib(La)TeX-Exporte verwendet haben, zu erleichtern.

auth.lower + shorttitle(3,3) + year bedeutet: Nachname des ersten Autors ohne Leerzeichen, in Kleinbuchstaben. Die ersten n (Standard: 3) Wörter des Titels, wobei die ersten m (Standard: 0) davon groß geschrieben werden. Veröffentlichungsjahr, falls vorhanden, ein Buchstabenanhang (a, b, c, etc.) im Falle eines Konflikts.

Wenn es komplizierter sein soll, können mehrere Muster durch ein Semikolon (;) oder senkrechten Strich (\|) getrennt festgelegt werden, wobei das erste angewendet wird, das eine nicht-leere Zeichenfolge liefert. Wenn alle eine leere Zeichenfolge zurückgeben, wird ein zufälliger Schlüssel generiert.

Nach einstellung die Liste refreshen.

![Refresh](Bilder/Refresh.png)

Wenn alles so eingestellt ist wie gewünscht, muss nur noch der Key verwendet werden:

`[@auth.lower + shorttitle(3,3) + year]`

Oder die gesetzte Tastenkombination und dann das Paper wählen.

### Parameter:

`auth` Parameter: Die ersten n (Standard: alle) Zeichen des Nachnamens des m-ten (Standard: ersten) Autors.

`authAuthEa` Parameter: Der Nachname der ersten beiden Autoren und „.ea“, wenn es mehr als zwei Autoren gibt.

`authEtAl` Parameter: Der Nachname des ersten Autors und der Nachname des zweiten Autors, wenn es zwei Autoren gibt, oder „EtAl“, wenn es mehr als zwei gibt. Dies ist ähnlich wie auth.etal. Der Unterschied besteht darin, dass die Autoren nicht durch „.“ getrennt sind und im Falle von mehr als 2 Autoren „EtAl“ statt „.etal“ hinzugefügt wird.

`authEtal2` Parameter: Der Nachname des ersten Autors und der Nachname des zweiten Autors, wenn es zwei Autoren gibt, oder „.etal“, wenn es mehr als zwei gibt.

´authForeIni\` Parameter: Der Anfangsbuchstabe des Vornamens des ersten Autors.

`authIni` Parameter: Der Anfang jedes Nachnamens der Autoren, wobei nicht mehr als n Zeichen (0 = alle) verwendet werden.

`authorIni` Parameter: Die ersten 5 Zeichen des Nachnamens des ersten Autors und die Initialen der Nachnamen der übrigen Autoren.

`authorLast` Parameter: Der Nachname des letzten Autors.

`authorLastForeIni` Parameter: Der Anfangsbuchstabe des Vornamens des letzten Autors.

`authorsAlpha` Parameter: Entspricht dem BibTeX-Stil „alpha“. Ein Autor: Die ersten drei Buchstaben des Nachnamens. Zwei bis vier Autoren: Anfangsbuchstaben der Nachnamen zusammengefügt. Mehr als vier Autoren: Anfangsbuchstaben der Nachnamen der ersten drei Autoren zusammengefügt. „+“ am Ende.

`authorsn` Parameter: Die Nachnamen der ersten n (Standard: alle) Autoren.

`authshort` Parameter: Der Nachname, wenn ein Autor/Editor angegeben ist; der erste Buchstabe der Nachnamen von bis zu drei Autoren, wenn mehr als ein Autor angegeben ist. Ein Pluszeichen wird hinzugefügt, wenn es mehr als drei Autoren gibt.

`creators` Parameter: Autor/Editor-Informationen.

`creatortypes` Parameter: Gibt eine durch Kommas getrennte Liste von Typ-Informationen der Ersteller für alle Ersteller des Elements in der Form \<1 oder 2\><Ersteller-Typ> zurück, wobei 1 oder 2 einen 1-teiligen oder 2-teiligen Ersteller bezeichnet, und Ersteller-Typ ist einer von {{% citekey-formatters/creatortypes %}}, oder primary für den Haupt-Ersteller-Typ des Zotero-Elements. Die Liste wird vom Elementtyp vorangestellt, sodass sie z.B. so aussehen könnte: audioRecording:2performer,2performer,1composer.

`date` Parameter: Das Datum der Veröffentlichung.

`extra` Parameter: Ein Pseudofeld aus dem Zusatzfeld. Z.B. wenn Sie Original date: 1970 in Ihrem Zusatzfeld haben, können Sie es als extra(originalDate) abrufen, oder tex.shortauthor: APA, welches Sie mit extra('tex.shortauthor') abrufen können. Jedes tex.-Feld wird erfasst, die anderen Felder können aus dieser Liste von Schlüsselnamen ausgewählt werden.

`firstpage` Die Nummer der ersten Seite der Veröffentlichung (Vorsicht: dies wird die niedrigste Zahl im Seitenfeld zurückgeben, da BibTeX 7,41,73--97 oder 43+ erlaubt).

`infix` Parameter: Eine Pseudo-Funktion, die das Zitationsschlüssel-Disambiguierungs-Infix mit einem sprintf-js-Formatspezifikator setzt, wenn ein Schlüssel generiert wird, der bereits existiert. Das Infix-Zeichen erscheint an der Stelle dieser Funktion in der Formel statt am Ende (wie postfix). Sie müssen genau einen der Platzhalter %(n)s (Nummer), %(a)s (Alpha, Kleinbuchstaben) oder %(A)s (Alpha, Großbuchstaben) einfügen. Für den Rest des Disambiguators können Sie Dinge wie Polsterung und zusätzlichen Text verwenden, wie sprintf-js es erlaubt. Mit start auf 1 gesetzt, wird der Disambiguator immer einbezogen, auch wenn kein Bedarf besteht, wenn keine Duplikate existieren. Das Standardformat ist %(a)s.

`inspireHep` Holt den Schlüssel von inspire-hep basierend auf DOI oder arXiv ID.

`item` Parameter: Gibt die interne Artikel-ID/Schlüssel zurück.

`journal` Parameter: Gibt die Zeitschriftenabkürzung oder, falls nicht vorhanden, den Zeitschriftentitel zurück. Wenn „automatische Zeitschriftenabkürzung“ in den BBT-Einstellungen aktiviert ist, wird derselbe Abkürzungsfilter verwendet, den Zotero in der Textverarbeitungseinbindung verwendet. Sie könnten den abbr-Filter darauf anwenden. Das Abkürzungsverhalten kann als abbrev+auto (Standard), auto, abbrev oder full/off angegeben werden.

`keyword` Parameter: Schlagwortnummer n. Hauptsächlich für die Kompatibilität mit älteren Versionen - die Reihenfolge der Schlagworte ist undefiniert.

`language` Parameter: Prüft, ob das Element die angegebene Sprache hat, und überspringt zum nächsten Muster, wenn nicht.

`lastpage` Die Nummer der letzten Seite der Veröffentlichung (Siehe den Hinweis zur firstpage).

`library` Gibt den Namen der freigegebenen Gruppenbibliothek zurück oder nichts, wenn das Element in Ihrer persönlichen Bibliothek ist.

`month` Der Monat der Veröffentlichung.

`origdate` Das Originaldatum der Veröffentlichung.

`origyear` Das Originaljahr der Veröffentlichung.

`postfix` Parameter: Eine Pseudo-Funktion, die das Zitationsschlüssel-Disambiguierungs-Postfix mit einem sprintf-js-Formatspezifikator setzt, wenn ein Schlüssel generiert wird, der bereits existiert. Fügt dem Zitationsschlüssel sonst keinen Text hinzu. Sie müssen genau einen der Platzhalter %(n)s (Nummer), %(a)s (Alpha, Kleinbuchstaben) oder %(A)s (Alpha, Großbuchstaben) einfügen. Für den Rest des Disambiguators können Sie Dinge wie Polsterung und zusätzlichen Text verwenden, wie sprintf-js es erlaubt. Mit start auf 1 gesetzt, wird der Disambiguator immer einbezogen, auch wenn kein Bedarf besteht, wenn keine Duplikate existieren. Das Standardformat ist %(a)s.

`shorttitle` Parameter: Die ersten n (Standard: 3) Wörter des Titels, wobei die ersten m (Standard: 0) davon groß geschrieben werden.

`shortyear` Die letzten 2 Ziffern des Veröffentlichungsjahres.

`title` Alle wichtigen Wörter des Titels groß schreiben und zusammenfügen. Zum Beispiel wird aus „An awesome paper on JabRef“ „AnAwesomePaperJabref“.

`type` Parameter: Ohne Argumente gibt er den Elementtyp zurück. Wenn Argumente übergeben werden, wird geprüft, ob das Element von einem der angegebenen Typen ist, und zum nächsten Muster übersprungen, wenn nicht, z.B. type(book) + veryshorttitle \| auth + year.

`veryshorttitle` Parameter: Die ersten n Wörter des Titels, wobei die ersten m davon groß geschrieben werden.

`year` Das Jahr der Veröffentlichung.

## Word

MIGRATION VON WORD/LIBREOFFICE

Lade better-bibtex-citekeys.csl CSL Style runter und installieren, wird Zotero die In-Text-Zitate als [@citekey] rendern, wenn Sie Zotero auffordern, das Literaturverzeichnis zu erstellen. Wenn Sie LaTeX benötigen, passen Sie den Stil entsprechend an. Anschließend können Sie Ihr Dokument durch pandoc oder ein ähnliches Tool laufen lassen, um LaTeX/Markdown zu erhalten.

![CSL](Bilder/csl.png)

Aktualisierung: pandoc unterstützt jetzt docx+citations als Eingabeformat und wird Ihre Word-Dokumente in pandoc-kompatibles Markdown mit Zitationen exportieren! Das sollte eine viel reibungslosere Erfahrung bieten:

`pandoc -f docx+citations -t markdown -i Testdokument.docx -o Testdokument.md`

Anleitung:

`https://retorque.re/zotero-better-bibtex/citing/migrating/index.html`

## Benutzung

Plugin in only office onstallieren Zotero API key erstellen
